// Теоретичне питання:
// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript
// Асинхронність у JS означає здатність мови виконувати декілька завдань одночасно,
// не блокуючи виконання іншого коду. JS однопоточний (може виконувати тільки одне завдання
// за раз), замість того, щоб чекати, поки завершиться трудомістке завдання JS використовує 
// зворотні виклики, обіцянки та async/await для ефективної обробки асинхронних операцій. 

// Практичне завдання:

const findBtn = document.getElementById('findBtn');
const addressinfoDiv = document.getElementById('addressInfo');

async function getIPAddress() {
    try {
        const ipResponse = await fetch('https://api.ipify.org/?format=json');
        const ipData = await ipResponse.json();

        const ipAddress = ipData.ip;
        const addressResponse = await fetch(`http://ip-api.com/json/${ipAddress}`);
        const addressData = await addressResponse.json();

        const {continent, country, region, city, district} = addressData;

        addressinfoDiv.textContent = `Continent: ${continent ||'not found'}, Country: ${country || 'not found'}, Region: ${region || 'not found'}, City: ${city || 'not found'}, District; ${district || 'not found'}`;

    } catch(error) {
        console.error(error);
        addressinfoDiv.textContent = 'Error occurred while retrieving address information.';
    }
}

findBtn.addEventListener('click', async () => {
    await getIPAddress();
});